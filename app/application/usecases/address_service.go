package usecases

import (
	"fias/domain/address/entity"
	"fias/domain/address/repository"
	cache "fias/infrastructure/interfaces"
)

type AddressService struct {
	addressRepository repository.AddressRepository
	houseRepository   repository.HouseRepository
	cache             cache.CacheInterface
}

func NewAddressService(addrRepo repository.AddressRepository, houseRepository repository.HouseRepository, cache cache.CacheInterface) *AddressService {
	return &AddressService{
		addressRepository: addrRepo,
		houseRepository:   houseRepository,
		cache:             cache,
	}
}

func (s *AddressService) GetByGuid(guid string) (*entity.AddrObject, error) {
	address := s.addressRepository.GetByGuid(guid)
	return &address, nil
}
func (a *AddressService) GetCities() ([]entity.AddrObject, error) {
	cities := a.addressRepository.GetCities()
	return cities, nil
}

func (a *AddressService) GetCitiesByTerm(term string, count int64) ([]entity.AddrObject, error) {
	cities := a.addressRepository.GetCitiesByTerm(term, count)
	return cities, nil
}

func (a *AddressService) GetStreetsByTerm(parentGuid string, term string, count int64) ([]entity.AddrObject, error) {
	streets := a.addressRepository.GetStreetsByTerm(parentGuid, term, count)
	return streets, nil
}

func (a *AddressService) GetHousesByTerm(parentGuid string, term string) ([]entity.HouseObject, error) {
	houses := a.houseRepository.GetByAddressGuid(parentGuid, term)
	return houses, nil
}
