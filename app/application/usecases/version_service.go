package usecases

import (
	"fias/domain/address/entity"
	"fias/domain/address/repository"
)

type VersionService struct {
	versionRepository repository.VersionRepository
}

func NewVersionService(versionRepo repository.VersionRepository) *VersionService {
	return &VersionService{
		versionRepository: versionRepo,
	}
}

func (v *VersionService) GetLastVersionInfo() *entity.Version {
	version := v.versionRepository.GetLastVersion()
	return version
}

func (v *VersionService) AddVersion(version int) error {
	versionEntity := entity.Version{Version: version}
	err := v.versionRepository.AddNewVersion(&versionEntity)
	if err != nil {
		return err
	}
	return nil
}
