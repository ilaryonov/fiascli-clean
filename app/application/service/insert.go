package service

import (
	"fias/domain/address/repository"
	"log"
	"os"
	"strconv"
)

func insertCollection(repo repository.InsertUpdateRepository, collection []interface{}, node interface{}, isFull bool) []interface{} {
	if collection == nil {
		collection = append(collection, node)
		return collection
	}
	if node == nil {
		repo.InsertUpdateCollection(collection, isFull)
		return collection[:0]
	}
	count, err := strconv.ParseInt(os.Getenv("COLLECTION_COUNT"), 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	if len(collection) <  int(count) {
		collection = append(collection, node)
		return collection
	} else {
		collection = append(collection, node)
		repo.InsertUpdateCollection(collection, isFull)
		return collection[:0]
	}
}