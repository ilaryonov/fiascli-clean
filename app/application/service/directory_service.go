package service

import (
	addressEntity "fias/domain/address/entity"
	"fias/domain/address/entity/file"
	"github.com/sirupsen/logrus"
	"regexp"
	"sync"
)

type DirectoryService struct {
	logger               logrus.FieldLogger
	downloadService      *DownloadService
	addressImportService *AddressImportService
	houseImportService   *HouseImportService
	IsFull               bool `default:"false"`
}

func NewDirectoryService(logger logrus.FieldLogger, addressImportService *AddressImportService, houseImportService *HouseImportService) *DirectoryService {
	return &DirectoryService{
		logger:          logger,
		downloadService: NewDownloadService(logger),
		addressImportService: addressImportService,
		houseImportService: houseImportService,
	}
}

func (d *DirectoryService) DownloadAndExtractFile(url string) *[]file.File {
	f, err := d.downloadService.DownloadFile(url)
	if err != nil {
		logrus.Fatal(err.Error())
	}
	extractedFiles, err := d.downloadService.Unzip(f)
	if err != nil {
		logrus.Fatal(err.Error())
	}
	return &extractedFiles
}

func (d *DirectoryService) ParseFiles(files *[]file.File) {
	var wg sync.WaitGroup
	for _, f := range *files {
		if r, err := regexp.MatchString(addressEntity.GetAddressXmlFile(), f.Path); err == nil && r {
			wg.Add(1)
			go d.addressImportService.Import(f.Path, &wg, d.IsFull)
		}
		if r, err := regexp.MatchString(addressEntity.GetHouseXmlFile(), f.Path); err == nil && r {
			wg.Add(1)
			go d.houseImportService.Import(f.Path, &wg, d.IsFull)
		}
	}
	wg.Wait()

}
