package helper

import (
	"crypto/tls"
	"encoding/json"
	"fias/domain/address/entity/file"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
)

const (
	allFiles = "GetAllDownloadFileInfo"
	lastFile = "GetLastDownloadFileInfo"
)

type FiasApi struct {
	logger *logrus.FieldLogger
}

func NewFiasApi(logger *logrus.FieldLogger) *FiasApi {
	return &FiasApi{
		logger: logger,
	}
}

func (f *FiasApi) GetAllDownloadFileInfo() []file.DownloadFileInfo {
	url := os.Getenv("FIAS_API_LINK") + allFiles
	var files []file.DownloadFileInfo
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	result, err := client.Get(url)
	if err != nil {
		log.Fatal("Error: " + err.Error())
	}
	json.NewDecoder(result.Body).Decode(&files)

	return files
}

func (f *FiasApi) GetLastDownloadFileInfo() file.DownloadFileInfo {
	url := os.Getenv("FIAS_API_LINK") + lastFile
	downloadFileInfo := file.DownloadFileInfo{}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	result, err := client.Get(url)
	if err != nil {
		log.Fatal("Error: " + err.Error())
	}
	json.NewDecoder(result.Body).Decode(&downloadFileInfo)

	return downloadFileInfo
}
