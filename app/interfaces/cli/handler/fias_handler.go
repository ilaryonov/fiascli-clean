package handler

import (
	"fias/application/service"
	"fias/application/usecases"
	"fias/helper"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

type FiasHandler struct {
	logger           logrus.FieldLogger
	versionService   *usecases.VersionService
	fiasApi          *helper.FiasApi
	directoryService *service.DirectoryService
}

func NewFiasHandler(logger logrus.FieldLogger, versionService *usecases.VersionService, fiasApi *helper.FiasApi, directory *service.DirectoryService) *FiasHandler {
	return &FiasHandler{
		logger:           logger,
		versionService:   versionService,
		fiasApi:          fiasApi,
		directoryService: directory,
	}
}

func (f *FiasHandler) CheckUpdates(c *cli.Context) error {
	v := f.versionService.GetLastVersionInfo()
	if v.Version > 0 {
		result := f.fiasApi.GetAllDownloadFileInfo()
		for _, file := range result {
			if file.VersionId > v.Version {
				xmlFiles := f.directoryService.DownloadAndExtractFile(file.FiasDeltaXmlUrl)
				f.directoryService.ParseFiles(xmlFiles)
				err := f.versionService.AddVersion(file.VersionId)
				if err != nil {
					f.logger.Error(err)
				}
			}
		}
	} else {
		f.directoryService.IsFull = true
		fileResult := f.fiasApi.GetLastDownloadFileInfo()
		if len(fileResult.FiasCompleteXmlUrl) > 0 {
			xmlFiles := f.directoryService.DownloadAndExtractFile(fileResult.FiasCompleteXmlUrl)
			f.directoryService.ParseFiles(xmlFiles)
			err := f.versionService.AddVersion(fileResult.VersionId)
			if err != nil {
				f.logger.Error(err)
			}
		}
	}
	return nil
}

func (f *FiasHandler) GetLastVersionInfo(c *cli.Context) error {
	v := f.versionService.GetLastVersionInfo()
	fmt.Println(v.Version)
	return nil
}
