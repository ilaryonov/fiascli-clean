package address_v1

import (
	"context"
	"errors"
	"fias/application/usecases"
	"fias/helper"
	"flag"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"sync"
)

type FiasServer struct {
	Server               *grpc.Server
	Logger               log.FieldLogger
	serverCert           string
	serverKey            string
	certificateAuthority string
	middleware           *helper.Middleware
}

func (s *FiasServer) Run() error {
	return errors.New("Error")
}

func NewGrpcServer(
	logger log.FieldLogger,
	addressService *usecases.AddressService,
) *FiasServer {

	gserver := grpc.NewServer()

	// Регистрация хэндлеров
	RegisterAddressHandlerServer(gserver, NewAddressHandler(logger, addressService))
	//v1.RegisterRequestThemeServiceServer(gserver, handler.NewThemeHandler(themeService, logger))

	return &FiasServer{
		Server:               gserver,
		Logger:               logger,
		serverCert:           "",
		serverKey:            "",
		certificateAuthority: "",
		middleware:           &helper.Middleware{},
	}
}

// Запуск grpc сервера
func (s *FiasServer) RunGrpcServer(wg *sync.WaitGroup) {
	// Слушаем grpc-порт из настроек
	port, _ := strconv.Atoi(os.Getenv("GRPC_PORT"))
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Println(err)
	}
	log.Printf("start grpc server port: %s", os.Getenv("GRPC_PORT"))
	s.Server.Serve(lis)
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("stopping grpc server...")
	s.Server.GracefulStop()
}

// Запуск proxy-сервера rest -> grpc
func (s *FiasServer) RunProxyServer(wg *sync.WaitGroup) {
	defer wg.Done()
	grpcPort, _ := strconv.Atoi(os.Getenv("GRPC_PORT"))
	var grpcServerEndpoint = flag.String("grpc-server-endpoint", fmt.Sprintf(":%d", grpcPort), "gRPC server endpoint")
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux(
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{}),
	)
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := RegisterAddressHandlerHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)
	if err != nil {
		log.Fatal("error reg endpoint", err)
	}
	restPort, _ := strconv.Atoi(os.Getenv("SERVER_PORT"))
	// Start HTTP server (and proxy calls to gRPC server endpoint)
	log.Println("Start Http server on port: ", restPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", restPort), mux))
}
