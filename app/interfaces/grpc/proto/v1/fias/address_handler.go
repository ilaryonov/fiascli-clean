package address_v1

import (
	"context"
	"fias/application/usecases"
	"fias/helper"
	"github.com/sirupsen/logrus"
)

type AddressHandler struct {
	logger         logrus.FieldLogger
	addressService *usecases.AddressService
	serializer     *helper.Serializer
}

func NewAddressHandler(logger logrus.FieldLogger, addressService *usecases.AddressService) *AddressHandler {
	return &AddressHandler{
		logger:         logger,
		addressService: addressService,
		serializer:     &helper.Serializer{},
	}
}

func (a *AddressHandler) GetByGuid(ctx context.Context, request *GuidRequest) (*AddressMessage, error) {
	add, err := a.addressService.GetByGuid(request.Guid)
	if err != nil {
		return nil, err
	}
	result := AddressMessage{
		Aoguid:     add.Aoguid,
		Aolevel:    add.Aolevel,
		Formalname: add.Formalname,
		Parentguid: add.Parentguid,
		Shortname:  add.Shortname,
		Postalcode: add.Postalcode,
	}
	return &result, err
}

func (a *AddressHandler) GetAllCities(ctx context.Context, request *EmptyRequest) (*Addresses, error) {
	cities, err := a.addressService.GetCities()
	if err != nil {
		return nil, err
	}
	addresses := Addresses{}
	err = a.serializer.ConvertStructToProto(cities, &addresses.Result)
	return &addresses, nil
}

func (a *AddressHandler) GetCitiesByTerm(ctx context.Context, request *TermRequest) (*Addresses, error) {
	if request.Count == 0 {
		request.Count = 10
	}
	cities, err := a.addressService.GetCitiesByTerm(request.Term, request.Count)
	if err != nil {
		return nil, err
	}
	addresses := Addresses{}
	err = a.serializer.ConvertStructToProto(cities, &addresses.Result)
	return &addresses, nil
}

func (a *AddressHandler) GetStreetsByTerm(ctx context.Context, request *StreetTermRequest) (*Addresses, error) {
	if request.Count == 0 {
		request.Count = 10
	}
	cities, err := a.addressService.GetStreetsByTerm(request.ParentGuid, request.Term, request.Count)
	if err != nil {
		return nil, err
	}
	addresses := Addresses{}
	err = a.serializer.ConvertStructToProto(cities, &addresses.Result)
	return &addresses, nil
}

func (a *AddressHandler) GetHouseByTerm(ctx context.Context, request *StreetTermRequest) (*Houses, error) {
	if request.Count == 0 {
		request.Count = 10
	}
	cities, err := a.addressService.GetHousesByTerm(request.ParentGuid, request.Term)
	if err != nil {
		return nil, err
	}
	houses := Houses{}
	err = a.serializer.ConvertStructToProto(cities, &houses.Result)
	return &houses, nil
}

func (a *AddressHandler) mustEmbedUnimplementedAddressHandlerServer() {
	panic("implement me")
}
