package mysql

import (
	"fias/domain/address/entity"
	"fias/domain/address/repository"
)

type HouseRepository struct {
	db           *Database
	defaultLimit int32
	defaultSort  string
	defaultOrder string
}

func NewHouseRepository(db *Database) repository.HouseRepository {
	return &HouseRepository{
		db,
		10,
		"formalname",
		"asc",
	}
}

func (h *HouseRepository) GetByAddressGuid(parentGuid string, term string) []entity.HouseObject {
	var houses []entity.HouseObject
	h.db.DB.Where("parentguid = ? AND housenum LIKE ?", parentGuid, term+"%").Order("housenum asc").Find(&houses)
	return houses
}

func (h *HouseRepository) InsertUpdateCollection(collection []interface{}, isFull bool) {
	tableName := entity.HouseObject{}.TableName()
	var aoguid []string
	var forInsert []interface{}

	if isFull {
		forInsert = collection
	} else {
		for _, item := range collection {
			aoguid = append(aoguid, item.(entity.HouseObject).Houseguid)
		}
		foundedAddresses := h.CheckByGuids(aoguid)

		for _, item := range collection {
			if len(foundedAddresses[item.(entity.HouseObject).Houseguid].Houseguid) > 0 {
				continue
			} else {
				forInsert = append(forInsert, item.(entity.HouseObject))
			}
		}
	}

	if len(forInsert) > 0 {
		h.db.BatchInsert(forInsert, tableName)
	}
}

func (hr *HouseRepository) CheckByGuids(guids []string) map[string]entity.HouseObject {
	var houses []entity.HouseObject
	result := make(map[string]entity.HouseObject)
	hr.db.DB.Select([]string{"houseguid"}).Where("houseguid IN (?)", guids).Find(&houses)
	for _, item := range houses {
		result[item.Houseguid] = item
	}
	return result
}