package mysql

import (
	addressEntity "fias/domain/address/entity"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"os"
	"strconv"
	"strings"
)

//DB object
type Database struct {
	DB *gorm.DB
}

/**
Конструктор инициализации репозиториев
*/
func NewDatabase(Dbdriver, dsn string) (*Database, error) {
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	logmode, err := strconv.Atoi(os.Getenv("DB_LOGMODE"))
	if err != nil {
		logmode = 0
	}
	if logmode == 1 {
		db.LogMode(true)
	}
	return &Database{
		DB: db,
	}, nil
}

// Возвращает объект соединения с БД
func (s *Database) GetDb() interface{} {
	return s.DB
}

// Закрываем коннект к бд
func (s *Database) Close() error {
	return s.DB.Close()
}

// Запустить автомиграции структуры БД
func (s *Database) MigrateStructures() error {
	db := s.DB.AutoMigrate(
		&addressEntity.AddrObject{},
		&addressEntity.HouseObject{},
		&addressEntity.Version{},
	)
	return db.Error
}

func (s *Database) BatchInsert(collection []interface{}, tableName string) error {
	mainObj := collection[0]
	mainScope := s.DB.NewScope(mainObj)
	mainFields := mainScope.Fields()
	quoted := make([]string, 0, len(mainFields))
	for i := range mainFields {
		// If primary key has blank value (0 for int, "" for string, nil for interface ...), skip it.
		// If field is ignore field, skip it.
		if (mainFields[i].IsPrimaryKey && mainFields[i].IsBlank) || (mainFields[i].IsIgnored) {
			continue
		}
		quoted = append(quoted, mainScope.Quote(mainFields[i].DBName))
	}

	placeholdersArr := make([]string, 0, len(collection))

	for _, obj := range collection {
		scope := s.DB.NewScope(obj)
		fields := scope.Fields()
		placeholders := make([]string, 0, len(fields))
		for i := range fields {
			if (fields[i].IsPrimaryKey && fields[i].IsBlank) || (fields[i].IsIgnored) {
				continue
			}
			placeholders = append(placeholders, scope.AddToVars(fields[i].Field.Interface()))
		}
		placeholdersStr := "(" + strings.Join(placeholders, ", ") + ")"
		placeholdersArr = append(placeholdersArr, placeholdersStr)
		// add real variables for the replacement of placeholders' '?' letter later.
		mainScope.SQLVars = append(mainScope.SQLVars, scope.SQLVars...)
	}

	mainScope.Raw(fmt.Sprintf("INSERT INTO %s (%s) VALUES %s",
		tableName,
		strings.Join(quoted, ", "),
		strings.Join(placeholdersArr, ", "),
	))

	if _, err := mainScope.SQLDB().Exec(mainScope.SQL, mainScope.SQLVars...); err != nil {
		return err
	}
	return nil
}