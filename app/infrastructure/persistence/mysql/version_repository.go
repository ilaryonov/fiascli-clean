package mysql

import (
	"fias/domain/address/entity"
	"fias/domain/address/repository"
	"github.com/jinzhu/gorm"
)

type VersionRepository struct {
	db *gorm.DB
}

func NewVersionRepository(db *gorm.DB) repository.VersionRepository {
	return &VersionRepository{
		db,
	}
}

func (v *VersionRepository) GetLastVersion() *entity.Version {
	version := entity.Version{}
	//v.DB.Create(&entity.Version{Version: 610})
	v.db.Last(&version)
	if version.Version <= 0 {
		return &entity.Version{}
	}
	return &version
}

func (v *VersionRepository) AddNewVersion(version *entity.Version) error {
	db := v.db.Create(version)
	return db.Error
}
