package mysql

import (
	"fias/domain/address/entity"
	"fias/domain/address/repository"
)

type AddressRepository struct {
	db           *Database
	defaultLimit int32
	defaultSort  string
	defaultOrder string
}

func NewAddressRepository(db *Database) repository.AddressRepository {
	return &AddressRepository{
		db,
		10,
		"formalname",
		"asc",
	}
}

func (a *AddressRepository) GetByFormalname(term string) (*entity.AddrObject, error) {
	panic("implement me")
}

func (a *AddressRepository) InsertUpdateCollection(collection []interface{}, isFull bool) {
	tableName := entity.AddrObject{}.TableName()
	var forInsert []interface{}
	if isFull {
		forInsert = collection
	} else {
		//TODO узкое место, тормозит выгрузка из-за проверок на наличие
		var aoguid []string

		for _, item := range collection {
			aoguid = append(aoguid, item.(entity.AddrObject).Aoguid)
		}
		foundedAddresses := a.CheckByGuids(aoguid)

		for _, item := range collection {
			if len(foundedAddresses[item.(entity.AddrObject).Aoguid].Aoguid) > 0 {
				/*addr := item.(entity.AddrObject)
				addr.ID = foundedAddresses[item.(entity.AddrObject).Aoguid].ID
				a.DB.Save(&addr)*/
			} else {
				forInsert = append(forInsert, item.(entity.AddrObject))
			}
		}
	}
	if len(forInsert) > 0 {
		a.db.BatchInsert(forInsert, tableName)
	}
}

func (a *AddressRepository) CheckByGuids(guids []string) map[string]entity.AddrObject {
	var addresses []entity.AddrObject
	result := make(map[string]entity.AddrObject)
	a.db.DB.Select([]string{"aoguid"}).Where("aoguid IN (?)", guids).Find(&addresses)
	for _, item := range addresses {
		result[item.Aoguid] = item
	}
	return result
}

func (a *AddressRepository) GetByGuid(guid string) entity.AddrObject {
	addr := entity.AddrObject{}
	a.db.DB.Where("aoguid = ?", guid).First(&addr)
	return addr
}

func (a AddressRepository) GetCityByFormalname(term string) (*entity.AddrObject, error) {
	panic("implement me")
}

func (a *AddressRepository) GetCities() []entity.AddrObject {
	var cities []entity.AddrObject
	a.db.DB.Debug().Where("aolevel IN (?) AND shortname = ?", []int{1, 4}, "г").Order("aolevel asc").Find(&cities)

	return cities
}

func (a *AddressRepository) GetCitiesByTerm(term string, count int64) []entity.AddrObject {
	var cities []entity.AddrObject
	a.db.DB.Where("aolevel IN (?) AND shortname = ? AND formalname LIKE ?", []int{1, 4}, "г", term+"%").Limit(count).Order("aolevel asc").Find(&cities)

	return cities
}

func (a *AddressRepository) GetStreetsByTerm(parentGuid string, term string, count int64) []entity.AddrObject {
	var streets []entity.AddrObject
	a.db.DB.Where("aolevel IN (?) AND parentguid = ? AND formalname LIKE ?", []int{7, 91}, parentGuid, term+"%").Limit(count).Order("aolevel asc").Find(&streets)
	return streets
}
