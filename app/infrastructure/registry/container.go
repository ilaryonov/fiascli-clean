package registry

import (
	"fias/application/service"
	"fias/application/usecases"
	"fias/domain/address/repository"
	"fias/helper"
	"fias/infrastructure/cache"
	"fias/infrastructure/interfaces"
	"fias/infrastructure/persistence/mysql"
	"fias/interfaces/cli/handler"
	address_v1 "fias/interfaces/grpc/proto/v1/fias"
	"fmt"
	"github.com/allegro/bigcache"
	_ "github.com/go-sql-driver/mysql"
	"github.com/sarulabs/di"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"os"
	"time"
)

type Container struct {
	ctn di.Container
}

// Конструктор, строит DI контейнеры
func NewContainer() (*Container, error) {
	builder, err := di.NewBuilder()
	if err != nil {
		return nil, err
	}

	if err := builder.Add([]di.Def{
		{ // logger
			Name:  "logger",
			Build: buildLoggerContainer,
		},
		{ //CLI SERVICES
			Name:  "cli",
			Build: buildCliCommands,
		},
		{ // Кэш
			Name:  "cache",
			Build: buildCacheContainer,
		},
		//REPOSITORIES
		{
			// Database structure
			Name:  "db",
			Build: buildDbContainer,
			Close: func(obj interface{}) error {
				return obj.(*mysql.Database).Close()
			},
		},
		{
			Name: "addressRepository",
			Build: func(ctn di.Container) (interface{}, error) {
				dbContainer := ctn.Get("db").(*mysql.Database)
				return mysql.NewAddressRepository(dbContainer), nil
			},
		},
		{
			Name: "houseRepository",
			Build: func(ctn di.Container) (interface{}, error) {
				dbContainer := ctn.Get("db").(*mysql.Database)
				return mysql.NewHouseRepository(dbContainer), nil
			},
		},
		{
			Name: "versionRepository",
			Build: func(ctn di.Container) (interface{}, error) {
				dbContainer := ctn.Get("db").(*mysql.Database)
				return mysql.NewVersionRepository(dbContainer.DB), nil
			},
		},
		//SERVICES
		{
			Name:  "versionService",
			Build: buildVersionService,
		},
		{
			Name:  "addressService",
			Build: buildAddressService,
		},
		{
			Name:  "fiasApiService",
			Build: buildFiasApiService,
		},
		{
			Name:  "directoryService",
			Build: buildDirectoryService,
		},
		{
			Name:  "addressImportService",
			Build: buildAddressImportService,
		},
		{
			Name:  "houseImportService",
			Build: buildHouseImportService,
		},
		//HANDLERS
		{
			Name:  "fiasCliHandler",
			Build: buildFiasCliHandler,
		},
		{ // Кэш
			Name:  "grpcServer",
			Build: buildGrpcServer,
		},
	}...); err != nil {
		return nil, err
	}

	return &Container{
		ctn: builder.Build(),
	}, nil
}

// Возвращает контейнер по названию
func (c *Container) Resolve(name string) interface{} {
	return c.ctn.Get(name)
}

// Очистка
func (c *Container) Clean() error {
	return c.ctn.Clean()
}

func buildDbContainer(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	dsn := os.Getenv("DB_USER") + ":" + os.Getenv("DB_PASSWORD") + "@tcp(" + os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT") + ")/" + os.Getenv("DB_NAME") + "?charset=utf8mb4&parseTime=True&loc=Local"
	//dsn := "host=" + os.Getenv("DB_HOST") + " port=" + os.Getenv("DB_PORT") + " user=" + os.Getenv("DB_USER") + " dbname=" + os.Getenv("DB_NAME") + " password=" + os.Getenv("DB_PASSWORD")
	repos, err := mysql.NewDatabase(os.Getenv("DB_DRIVER"), dsn)
	if err != nil {
		logger.Error(fmt.Sprintf("DB error connection. Dsn: %s. Details: %s", dsn, err))
		return &mysql.Database{DB: nil}, nil
	}
	err = repos.MigrateStructures()
	if err != nil {
		logger.Error(fmt.Sprintf("DB migrations error: %s", err))
		return &mysql.Database{DB: nil}, nil
	}
	return repos, err
}

// Билд коннекта к БД
func buildLoggerContainer(ctn di.Container) (interface{}, error) {
	logger := log.Logger{}
	logger.SetFormatter(&log.JSONFormatter{})
	logger.SetOutput(os.Stdout)
	logger.Info("run grpc server")
	logger.SetLevel(log.ErrorLevel)
	return &logger, nil
}

// Билд консльного сервиса
func buildCliCommands(ctn di.Container) (interface{}, error) {
	cliHandler := ctn.Get("fiasCliHandler").(*handler.FiasHandler)
	app := cli.NewApp()
	app.Usage = "collection of workers"
	app.Commands = []*cli.Command{
		{
			Name:   "version",
			Usage:  "fias version",
			Action: cliHandler.GetLastVersionInfo,
		},
		{
			Name:   "checkupdates",
			Usage:  "fias run full import or delta's",
			Action: cliHandler.CheckUpdates,
		},
	}
	return app, nil
}

func buildVersionService(ctn di.Container) (interface{}, error) {
	versionRepo := ctn.Get("versionRepository").(repository.VersionRepository)
	versionService := usecases.NewVersionService(versionRepo)
	return versionService, nil
}
func buildAddressService(ctn di.Container) (interface{}, error) {
	addressRepo := ctn.Get("addressRepository").(repository.AddressRepository)
	houseRepo := ctn.Get("houseRepository").(repository.HouseRepository)
	cache := ctn.Get("cache").(interfaces.CacheInterface)

	addressService := usecases.NewAddressService(addressRepo, houseRepo, cache)
	return addressService, nil
}
func buildFiasApiService(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	//addressService := ctn.Get("addressService").(*usecases.AddressService)
	//versionService := ctn.Get("versionService").(*usecases.VersionService)
	apiService := helper.NewFiasApi(&logger)
	return apiService, nil
}
func buildFiasCliHandler(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	apiService := helper.NewFiasApi(&logger)
	versionService := ctn.Get("versionService").(*usecases.VersionService)
	directoryService := ctn.Get("directoryService").(*service.DirectoryService)
	handl := handler.NewFiasHandler(logger, versionService, apiService, directoryService)
	return handl, nil
}
func buildGrpcServer(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	addressService := ctn.Get("addressService").(*usecases.AddressService)
	handl := address_v1.NewGrpcServer(logger, addressService)
	return handl, nil
}
func buildDirectoryService(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	addressImport := ctn.Get("addressImportService").(*service.AddressImportService)
	houseImport := ctn.Get("houseImportService").(*service.HouseImportService)
	directory := service.NewDirectoryService(logger, addressImport, houseImport)
	return directory, nil
}
func buildAddressImportService(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	addressRepo := ctn.Get("addressRepository").(repository.AddressRepository)
	addressImport := service.NewAddressImportService(addressRepo, logger)
	return addressImport, nil
}
func buildHouseImportService(ctn di.Container) (interface{}, error) {
	logger := ctn.Get("logger").(log.FieldLogger)
	houseRepo := ctn.Get("houseRepository").(repository.HouseRepository)
	addressImport := service.NewHouseImportService(houseRepo, logger)
	return addressImport, nil
}
func buildCacheContainer(ctn di.Container) (interface{}, error) {
	config := bigcache.Config{
		Shards:             1024,
		LifeWindow:         10 * time.Minute,
		CleanWindow:        10 * time.Minute,
		MaxEntriesInWindow: 1000 * 10 * 60,
		MaxEntrySize:       500,
		Verbose:            true,
		HardMaxCacheSize:   8192,
		OnRemove:           nil,
		OnRemoveWithReason: nil,
	}
	bigCache, _ := bigcache.NewBigCache(config)
	cacheStruct := cache.NewBigCache(bigCache)
	return cacheStruct, nil
}
