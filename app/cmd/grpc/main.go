package main

import (
	"fias/infrastructure/persistence/mysql"
	"fias/infrastructure/registry"
	address_v1 "fias/interfaces/grpc/proto/v1/fias"
	log "github.com/sirupsen/logrus"
	"sync"
)

func main() {
	// Инициализация контейнера зависимостей
	ctn, err := registry.NewContainer()
	if err != nil {
		log.Fatalf("failed to build container: %v", err)
	}
	// Получение лог-сервиса из контейнера
	logger := ctn.Resolve("logger").(log.FieldLogger)
	// Получение объекта для работы с БД из контейнера
	database := ctn.Resolve("db").(*mysql.Database)
	if database.DB != nil {
		if err != nil {
			logger.Error(err.Error())
		}
	}
	// Запуск grpc сервера
	server := ctn.Resolve("grpcServer").(*address_v1.FiasServer)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go server.RunGrpcServer(&wg)
	wg.Add(1)
	go server.RunProxyServer(&wg)
	wg.Wait()
	// Очистка контейнера
	ctn.Clean()
}
