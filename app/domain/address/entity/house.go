package entity

type HouseObject struct {
	ID         uint   `gorm:"primary_key"`
	Parentguid string `xml:"AOGUID,attr" gorm:"index:parent_guid,parent_num"`
	Houseguid  string `xml:"HOUSEGUID,attr" gorm:"index:house_guid,parent_num"`
	Housenum   string `xml:"HOUSENUM,attr" gorm:"index:number,parent_num"`
	Buildnum   string `xml:"BUILDNUM,attr"`
	Structnum  string `xml:"STRUCTNUM,attr"`
	Postalcode string `xml:"POSTALCODE,attr"`
	EndDate    string `xml:"ENDDATE,attr"`
}

type HouseObjects struct {
	Object []HouseObjects
}

func GetHouseXmlFile() string {
	return "AS_HOUSE_"
}

func (o HouseObject) TableName() string {
	return "fias_house"
}
