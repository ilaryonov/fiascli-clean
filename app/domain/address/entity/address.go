package entity

type AddrObject struct {
	ID        uint `gorm:"primary_key"`
	Aoguid     string `xml:"AOGUID,attr" gorm:"primary_key"`
	Shortname  string `xml:"SHORTNAME,attr" gorm:"index:city"`
	Aolevel    string `xml:"AOLEVEL,attr" gorm:"index:city,street"`
	Parentguid string `xml:"PARENTGUID,attr" gorm:"index:parent,street"`
	Formalname string `xml:"FORMALNAME,attr"`
	Offname    string `xml:"OFFNAME,attr"`
	Postalcode string `xml:"POSTALCODE,attr"`
	Actstatus  string `xml:"ACTSTATUS,attr"`
}

func GetAddressXmlFile() string {
	return "AS_ADDROBJ_"
}

func (a AddrObject) TableName() string {
	return "fias_address"
}
