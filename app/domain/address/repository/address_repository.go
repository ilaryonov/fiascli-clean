package repository

import "fias/domain/address/entity"

type AddressRepository interface {
	GetByFormalname(term string) (*entity.AddrObject, error)
	GetCityByFormalname(term string) (*entity.AddrObject, error)
	InsertUpdateCollection(collection []interface{}, isFull bool)
	GetByGuid(guid string) entity.AddrObject
	GetCities() []entity.AddrObject
	GetCitiesByTerm(term string, count int64) []entity.AddrObject
	GetStreetsByTerm(parentGuid string, term string, count int64) []entity.AddrObject
}
