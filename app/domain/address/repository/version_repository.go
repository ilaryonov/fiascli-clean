package repository

import "fias/domain/address/entity"

type VersionRepository interface {
	GetLastVersion() *entity.Version
	AddNewVersion(version *entity.Version) error
}
