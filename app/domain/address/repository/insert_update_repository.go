package repository

type InsertUpdateRepository interface {
	InsertUpdateCollection(collection []interface{}, isFull bool)
}
