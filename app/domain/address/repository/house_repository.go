package repository

import "fias/domain/address/entity"

type HouseRepository interface {
	GetByAddressGuid(parentGuid string, term string) []entity.HouseObject
	InsertUpdateCollection(collection []interface{}, isFull bool)
}
