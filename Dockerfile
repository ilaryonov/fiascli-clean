FROM golang:alpine as builder

# Cache go modules
ENV GO111MODULE=on
RUN mkdir /app
WORKDIR /app
COPY ./app .

RUN go clean --modcache
RUN go mod download

RUN CGO_ENABLED=0 go build -o grpc /app/cmd/grpc/main.go && \
    CGO_ENABLED=0 go build -o cli /app/cmd/cli/main.go

FROM scratch
# Copy our static executable.
COPY --from=builder /app/grpc /grpc
COPY --from=builder /app/cli /cli
# Run the hello binary.
ENTRYPOINT ["/grpc"]