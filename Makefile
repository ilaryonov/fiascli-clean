import:
	go build -o fiascli && ./fiascli checkupdates
grpcGenGateway:
	protoc -I. -I$GOPATH/src -I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis --go_out=. --go-grpc_out="./" "interfaces/grpc/proto/v1/fias/address.proto" && \
	protoc -I. -I$GOPATH/src -I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis --grpc-gateway_out="./" "interfaces/grpc/proto/v1/fias/address.proto" && \
	protoc -I. -I$GOPATH/src -I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis --openapiv2_out "./" --openapiv2_opt logtostderr=true "interfaces/grpc/proto/v1/fias/address.proto"
grpcOnly:
	protoc -I. -I$GOPATH/src -I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis --go_out=. --go-grpc_out="./" "interfaces/grpc/proto/v1/fias/address.proto"

restOnly:
	protoc -I. -I$GOPATH/src -I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis --grpc-gateway_out="./" "interfaces/grpc/proto/v1/fias/address.proto"
swaggerOnly:
	protoc -I. -I$GOPATH/src -I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis --openapiv2_out "./" --openapiv2_opt logtostderr=true "interfaces/grpc/proto/v1/fias/address.proto"


